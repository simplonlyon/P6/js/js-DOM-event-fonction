"use strict"; // on active le mode strict de JS
// on sélectionne les éléments statiques de notre doculent HTML
let input = document.querySelector('#input'); // c'est le champs de texte dans lequel on entre la description du todo
let addBtn = document.querySelector('#add'); // c'est le bouton avec lequel on ajoute un todo dans la liste
let removeBtn = document.querySelector('#remove'); // c'est le bouton avec lequel on enlève les todo terminés

/**
 * Cette fonction à pour rôle d'ajouter un todo dans notre liste à partir du texte qu'il doit contenir.
 * @param {string} text la description du todo.
 */
function addTodo(text) {
  let ul = document.querySelector('#list'); // on sélectionne la liste, on ne le fait pas à l'extérieur de la fonction car elle agit dessus. On fait TOUJOURS en sorte qu'une fonction ne dépende que de ses paramètres pour pouvoir marcher, ça à l'avantage de rendre celle-ci plus facile à déplacer dans le code voir vers un autre projet.
  let li = document.createElement('li'); // on créé l'item de notre liste
  let p = document.createElement('p'); // on créé le paragraphe qui va contenir le texte de notre todo
  let checkbox = document.createElement('input'); // on créé un input...
  checkbox.setAttribute('type', 'checkbox'); // ...et on en fait une checkbox en ajoutant l'attribut type="checkbox"

  let btnSuppr = document.createElement('button');
  btnSuppr.textContent = 'x';
  btnSuppr.addEventListener('click', function() {
    li.remove();
  });

  ul.appendChild(li); // on insère l'item dans la liste (li dans ul)
  li.appendChild(checkbox); // on insère la checkbox dans l'item (input dans li)
  li.appendChild(p); // on insère le paragraphe dans l'item (p dans li)
  li.appendChild(btnSuppr);

  p.textContent = text; // on ajoute la description du todo dans le paragraphe

  /* 
  exemple, lorsque l'on appelle addTodo('un truc à faire') la liste va prendre cette forme :
  <ul>
    <li>
      <input type="checkbox">
      <p>un truc à faire</p>
    </li>
  </ul>
  */
}

addBtn.addEventListener('click', function(){ // on écoute l'événement "click" sur le bouton d'ajout du todo (qu'on sélectionné sur la ligne n°3)
  if (input.value !== '') { // on vérifie si notre input n'est pas vide, la partie `!== ''` n'est pas nécessaire car une string vide est évaluée a false (cf Boolean Evaluation dans le support sur JS)
    addTodo(input.value); // on ajoute un todo dans notre liste
    input.value = ''; // on réinitialise l'input
  }
  input.focus(); // on met le focus sur l'input pour rendre la todolist plus pratique ( c'est quoi le focus ? https://www.developpez.net/forums/d409609/java/interfaces-graphiques-java/awt-swing/c-quoi-focus/ )
});

removeBtn.addEventListener('click', function(){ // on écoute l'événement "click" sur le bouton de suppression des todos (qu'on sélectionné sur la ligne n°4)
  let ul = document.querySelector('#list'); // on selectionne la liste
  for (let index = 0; index < ul.children.length; index++) { // ul.children est un tableau qui contient tout les enfants (li) notre liste, on utilise ensuite une boucle for pour parcourir ce tableau
    if (ul.children[index].firstChild.checked === true) { // ul.children[index] est l'enfant actuellement (li) parcouru par notre boucle, firstchild est le premier enfant du li, à savoir la checkbox. une checkbox possède la propriété booléenne `checked` qui représente l'état de celle-ci (cochée, ou décochée). Pour résumer : on vérifie si la checkbox du todo actuellement parcouru par notre boucle est cochée.
      ul.children[index].remove(); // si c'est bien le cas, on supprime l'élément li du DOM avec la fonction `remove()`...
      index--; //...et on décrémente l'index pour ne pas sauter d'élément, explications :
      /*
        voici un tableau dont on souhaite supprimer les éléments qui ont pour valeur `true` présenté sous la forme `index:valeur` :
        |0:false|1:true|2:true|3:false| on cherche donc à supprimer les éléments se trouvant aux index 1 et 2, pour se faire on va donc utiliser une boucle for en partant de 0, la position de l'index est simbolisée par ▼


         ▼
        |0:false|1:true|2:true|3:false| première itération, la valeur est false on ne fait rien.
                 ▼
        |0:false|1:true|2:true|3:false| seconde itération, la valeur est true on supprime donc l'élément. Voici notre nouveau tableau :
        |0:false|1:true|2:false| les éléments du tableau on été décalés vers la gauche, continuons nos itérations :
                        ▼
        |0:false|1:true|2:false| notre index à de nouveau été incrémenté, nous avons sauté une valeur... Voici notre tableau final :
        |0:false|1:true|2:false|


        pour éviter ce problème, nous devons décaler notre index vers gauche également, pour ce faire on va donc décrémenter celui-ci lorsqu'on supprime l'élément :
         ▼
        |0:false|1:true|2:true|3:false| première itération, la valeur est false on ne fait rien.
                 ▼
        |0:false|1:true|2:true|3:false| seconde itération, la valeur est true on supprime donc l'élément ET on décrémente l'index, voici notre nouveau tableau :
        |0:false|1:true|2:false| continuons notre itération :
                 ▼
        |0:false|1:true|2:false| cette fois-ci, on retrouve bien l'élément que l'on doit supprimer, on le supprime et on décrémente à nouveau l'index. Voici le nouveau tableau :
        |0:false|1:false| continuons l'itération :
                 ▼
        |0:false|1:false| on est bien entrain de scruter la valeur suivante et cette fois-ci on ne fait rien. Nous sommes au bout du tableau, la boucle s'arrête. voici le tableau final :
        |0:false|1:false| victoire !


        conclusion : lorsqu'on boucle sur un tableau que l'on modifie durant celle-ci, il faut prendre en compte les décalages d'index.
      */
    }
  }
});


function removeTodos() {
  let ul = document.querySelector('#list');
  let li = ul.querySelectorAll('li');

  for(let item of li) {
    let checkbox = item.querySelector('input[type="checkbox"]');
    if(checkbox.checked) {
      item.remove();
    }
  }

}